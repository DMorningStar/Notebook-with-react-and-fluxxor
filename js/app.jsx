var React = require("react"),
    Fluxxor = require("fluxxor");

window.React = React;

var constants = {
    ADD_NOTE: "ADD_NOTE",
    UPDATE_NOTE: "UPDATE_NOTE",
    DELETE_NOTE: "DELETE_NOTE"
};

var NotesStore = Fluxxor.createStore({
    initialize: function () {
        this.noteId = 0;
        this.notes = {};
        if(localStorage["notes.id"] != undefined) {
            this.noteId = parseInt(localStorage["notes.id"]);
        }

        this.bindActions(
            constants.ADD_NOTE, this.onAddNote,
            constants.UPDATE_NOTE, this.onUpdateNote,
            constants.DELETE_NOTE, this.onDeleteNote
        );
    },

    onAddNote: function (payload) {
        var id = this._nextNoteId();
        var note = {
            id: id,
            text: payload.text,
            creationDate: new Date()
        };
        //localStorage["notes." + id] = JSON.stringify(note);
        this.notes[id] = note;
        localStorage["notes"] = JSON.stringify(this.notes);
        console.log(JSON.stringify(this.notes));
        localStorage["notes.id"] = id;
        this.emit("change");
    },

    onUpdateNote: function (payload) {
        var id = payload.id;
        /*var note = JSON.parse(localStorage["notes." +id]);
        note.text = payload.text;
        localStorage["notes." + id] = JSON.stringify(note);*/
        this.notes[id].text = payload.text;
        localStorage["notes"] = JSON.stringify(this.notes);
        this.emit("change");
    },

    onDeleteNote: function (payload) {
        //delete localStorage["notes." + payload.id];
        delete this.notes[payload.id];
        localStorage["notes"] = JSON.stringify(this.notes);
        this.emit("change");
    },

    getState: function () {
        /*var notes = {};
        for(var i = 0; i < this.noteId; i++) {
            if(localStorage["notes." + i] != undefined) {
                notes[i] = JSON.parse(localStorage["notes." + i]);
            }
        }*/
        if(localStorage["notes"] != undefined && parseInt(localStorage["notes.id"]) > 0) {
            this.notes = JSON.parse(localStorage["notes"]);
        }
        console.log(this.notes);
        return {
            notes: this.notes
        };
    },

    _nextNoteId: function () {
        return ++this.noteId;
    }
});

var actions = {

    addNote: function (text) {
        this.dispatch(constants.ADD_NOTE, {text: text});
    },

    updateNote: function (id, text) {
        this.dispatch(constants.UPDATE_NOTE, {id: id, text: text});
    },

    deleteNote: function(id) {
        this.dispatch(constants.DELETE_NOTE, {id: id});
    }
};

var stores = {
    NotesStore: new NotesStore()
};

var flux = new Fluxxor.Flux(stores, actions);

window.flux = flux;

flux.on("dispatch", function(type, payload) {
   if(console && console.log)  {
       console.log("[Dispatch]", type, payload);
   }
});

var FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

var Application = React.createClass({
    mixins: [FluxMixin, StoreWatchMixin("NotesStore")],

    getInitialState: function() {
        return {newNoteText: ""};
    },

    getStateFromFlux:  function() {
        var flux = this.getFlux();
        return flux.store("NotesStore").getState();
    },

    render: function() {
        var notes = this.state.notes;

        return (
            <div>
                {Object.keys(notes).map(function(id) {
                    return <NoteItem note={notes[id]}/>
                })}
                <form className="form-group" id="main_form" onSubmit={this.onSubmitForm}>
                    <textarea className="form-control" form="main_form" rows="4" cols="50" name="text"
                              value={this.state.newNoteText}
                              onChange={this.handleNoteTextChange} />
                    <input className="btn btn-primary" type="submit" value="Add Note"/>
                </form>
            </div>
        );
    },

    handleNoteTextChange: function(e) {
        console.log("Change state");
        this.setState({newNoteText: e.target.value});
    },

    onSubmitForm: function(e) {
        e.preventDefault();
        console.log(e);
        console.log(this.state.newNoteText);
        if(this.state.newNoteText.trim()) {
            console.log("onSubmitForm");
            this.getFlux().actions.addNote(this.state.newNoteText);
            this.setState({newNoteText: ""});
        }
    }

});

var NoteItem = React.createClass({

    mixins: [FluxMixin],

    propTypes: {
        note: React.PropTypes.object.isRequired
    },

    getInitialState: function() {
        return {
            edit: false,
            newNoteText: ""
        };
    },

    render: function() {
        if(this.state.edit) {
            return (
                <div class="from-group" id={this.props.note.id}>
                    <textarea className="form-control" rows="4" cols="50" name="text"
                              value={this.state.newNoteText}
                              onChange={this.handleTextChange}  />
                    <br/>
                    <input type="button" className="btn btn-danger" onClick={this.onUpdate} value="Update"/>
                </div>
            );
        } else {
            return (
                <div id={this.props.note.id} style={{"marginBottom": "5px"}}>
                    <p className="text-info" dangerouslySetInnerHTML={{__html:
                        this.props.note.text.replace(new RegExp("\n", 'g'), "<br />")}}></p>
                    <span style={{color: "red"}}>{this.props.note.creationDate}</span>
                    <br/>
                    <input type="button" className="btn btn-default" onClick={this.onDelete} value="Delete"/>
                    <input type="button" className="btn btn-default" onClick={this.switchState} value="Update"/>
                </div>
            );
        }
    },

    handleTextChange: function(e) {
       this.setState( {newNoteText: e.target.value});
    },

    switchState: function() {
        this.setState({
            edit: !this.state.edit,
            newNoteText: this.props.note.text
        });
    },

    onUpdate: function() {
        this.getFlux().actions.updateNote(this.props.note.id, this.state.newNoteText);
        this.switchState();
    },

    onDelete: function() {
        console.log(this.mixin);
        this.getFlux().actions.deleteNote(this.props.note.id);
    }
});

React.render(<Application flux={flux} />, document.getElementById("app"));